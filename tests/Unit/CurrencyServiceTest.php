<?php

namespace Tests;

use App\Services\CurrencyService;
use App\Models\ExchangeRate;
use Laravel\Lumen\Testing\TestCase as BaseTestCase;

abstract class CurrencyServiceTest extends BaseTestCase
{
    public function testGetExchangeRatesReturnsFromDatabaseIfAvailable()
    {
        // Mock the ExchangeRate model to return some sample data
        $exchangeRateModel = $this->getMockBuilder(ExchangeRate::class)
            ->disableOriginalConstructor()
            ->getMock();
        $exchangeRateModel->expects($this->once())
            ->method('whereDate')
            ->willReturnSelf();
        $exchangeRateModel->expects($this->once())
            ->method('get')
            ->willReturn(collect([['num_code' => '036', 'char_code' => 'AUD']]));

        // Create an instance of CurrencyService with the mocked model
        $currencyService = new CurrencyService($exchangeRateModel);

        // Call the getExchangeRates method
        $exchangeRates = $currencyService->fetchRates();

        // Assert that the exchange rates are returned from the database
        $this->assertEquals([['num_code' => '036', 'char_code' => 'AUD']], $exchangeRates);
    }

}
