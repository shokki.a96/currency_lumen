<?php

namespace App\Http\Controllers;

use App\Services\CurrencyService;
use Illuminate\Http\Request;
use App\Services\ExchangeRate;
use Carbon\Carbon;
use App\Models\ExchangeRate as ExchangeRateModel;

use Laravel\Lumen\Routing\Controller as BaseController;

class CurrencyController extends BaseController
{
  
    public function __construct(
        protected ExchangeRate $exchangeRateService, 
        protected CurrencyService $currencyService)
    {   
    }

    public function getExchangeRates()
    {
        $today = Carbon::today()->toDateString();

        $exchangeRates = $this->exchangeRateService->getExchangeRatesByDate($today);
        // return ExchangeRateModel::get();
        if ($exchangeRates->isNotEmpty()) {
            return response($exchangeRates);
        }

        $exchangeRates = $this->currencyService->fetchRates();

        if (!empty($exchangeRates)) {
            $this->exchangeRateService->storeExchangeRates($exchangeRates);
        }

        return response($exchangeRates);
    }
}
