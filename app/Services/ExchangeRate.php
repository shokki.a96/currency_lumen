<?php

namespace App\Services;

use App\Models\ExchangeRate as ExchangeRateModel;

use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;

class ExchangeRate
{

    public function getExchangeRatesByDate($date)
    {
        return ExchangeRateModel::whereDate('date', $date)->get();
    }

    public function storeExchangeRates($exchangeRates)
    {
        ExchangeRateModel::insert($exchangeRates);
    }
}
