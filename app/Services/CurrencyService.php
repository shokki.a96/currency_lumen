<?php

namespace App\Services;

use GuzzleHttp\Client;
use Carbon\Carbon;
class CurrencyService
{
    public function fetchRates()
    {
        $client = new Client();
        try {
            $url = env('CURRENCY_EXCHANGE_URL');
            $response = $client->get($url);

            if ($response->getStatusCode() === 200) {
                $body = $response->getBody()->getContents();
                $xml = simplexml_load_string($body);

                $valCursDate = (string)$xml['Date'];

                $formattedDate = Carbon::createFromFormat('d.m.Y', $valCursDate)->toDateString();

                $rates = [];
                foreach ($xml->Valute as $valute) {
                    $rates[] = [
                        'num_code' => (string)$valute->NumCode,
                        'char_code' => (string)$valute->CharCode,
                        'nominal' => (int)$valute->Nominal,
                        'name' => (string)$valute->Name,
                        'value' => str_replace(',', '.', (string)$valute->Value),
                        'vunitrate' => str_replace(',', '.', (string)$valute->VunitRate),
                        'date' => $formattedDate,
                    ];
                }

                return $rates;
            }
        } catch (\Exception $e) {
            \Log::error('Exception occurred while fetching exchange rates: ' . $e->getMessage());
        }

        return [];
    }
}
