<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExchangeRate extends Model {

    protected $fillable = [
        'num_code', 'char_code', 'nominal', 'name', 'value', 'vunitrate', 'date'
    ];

   
}
